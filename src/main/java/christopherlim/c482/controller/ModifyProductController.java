package christopherlim.c482.controller;

import christopherlim.c482.helper.Exceptions;
import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.Inventory;
import christopherlim.c482.model.Part;
import christopherlim.c482.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * Controller for Modify Product
 * @author Christopher Lim
 */
public class ModifyProductController {
    @FXML
    private TextField productIDText, productNameText, productInvText, productPriceOrCostText, productInvMaxText, productInvMinText, partsSearchBox;
    @FXML
    private TableView<Part> availablePartsTable;
    @FXML
    private TableView<Part> associatedPartsTable;

    private Product modifiedProduct;
    private int productIndex;

    /**
     * Initialized based on the Product provided in.
     * @param productToModify
     * @param productIdx
     */
    public void initForm(Product productToModify, int productIdx) {
        productIDText.setText(Integer.toString(productToModify.getId()));
        productNameText.setText(productToModify.getName());
        productInvText.setText(Integer.toString(productToModify.getStock()));
        productPriceOrCostText.setText(Double.toString(productToModify.getPrice()));
        productInvMaxText.setText(Integer.toString(productToModify.getMax()));
        productInvMinText.setText(Integer.toString(productToModify.getMin()));

        availablePartsTable.setItems(Inventory.getAllParts());
        Utilities.formatTable(availablePartsTable);
        associatedPartsTable.setItems(productToModify.getAllAssociatedParts());
        Utilities.formatTable(associatedPartsTable);

        modifiedProduct = Utilities.copyProduct(productToModify);
        productIndex = productIdx;
    }

    /**
     * Filter parts for Parts Id and Parts Name
     */
    public void filterParts() {
        String searchText = partsSearchBox.getText();

        try {
            int searchID = Integer.parseInt(searchText);
            filterPartsByID(searchID);
        }
        catch (NumberFormatException e) {
            filterPartsByName(searchText);
        }
    }

    /**
     * Filters by available Parts name
     * @param searchName
     */
    private void filterPartsByName(String searchName) {

        ObservableList<Part> foundParts;
        try {
            foundParts = Inventory.lookupPart(searchName);
            availablePartsTable.setItems(foundParts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }

    /**
     * Filters by available Parts Id
     * @param searchID
     */
    private void filterPartsByID(int searchID) {
        Part foundPart;

        ObservableList<Part> foundParts = FXCollections.observableArrayList();
        try {
            foundPart = Inventory.lookupPart(searchID);
            foundParts.add(foundPart);
            availablePartsTable.setItems(foundParts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }

    /**
     * Select part to add to parts table.
     */
    public void addAssociatedPart() {
        Part associatedPart = availablePartsTable.getSelectionModel().getSelectedItem();
        modifiedProduct.addAssociatedPart(associatedPart);
        associatedPartsTable.setItems(modifiedProduct.getAllAssociatedParts());
    }

    /**
     * Removes part from parts table.
     */
    public void removeAssociatedPart() {
        Part partToRemove = associatedPartsTable.getSelectionModel().getSelectedItem();

        if (Utilities.confirmUserAction("part disassociation")) {
            modifiedProduct.deleteAssociatedPart(partToRemove);
            associatedPartsTable.setItems(modifiedProduct.getAllAssociatedParts());
        }
    }

    /**
     * Updates Product in Inventory.
     * @param event
     */
    public void modifyProduct(ActionEvent event) {
        try {
            modifiedProduct.setName(productNameText.getText());
            modifiedProduct.setStock(Integer.parseInt(productInvText.getText()));
            modifiedProduct.setPrice(Double.parseDouble(productPriceOrCostText.getText()));
            modifiedProduct.setMax(Integer.parseInt(productInvMaxText.getText()));
            modifiedProduct.setMin(Integer.parseInt(productInvMinText.getText()));

            Utilities.validateProductInventory(modifiedProduct);

            Inventory.updateProduct(productIndex, modifiedProduct);
            Utilities.navigateToNewPage(event, "/christopherlim/c482/view/MainScreen.fxml");
        }
        catch (NumberFormatException e) {
            Exceptions.displayNumberFormattingErrorAlert(e);
        }
        catch (Exception e) {
            Exceptions.displayErrorAlert(e);
        }
    }

    /**
     * Goes back to the Main Screen
     * @param event
     */
    public void returnToMainPage(ActionEvent event) {
        Utilities.navigateToNewPage(event, "/christopherlim/c482/view/MainScreen.fxml");
    }
}
