package christopherlim.c482.controller;

import christopherlim.c482.helper.Exceptions;
import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.InHouse;
import christopherlim.c482.model.Inventory;
import christopherlim.c482.model.Outsourced;
import christopherlim.c482.model.Part;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * Controller for Modify Part
 * @author Christopher Lim
 */
public class ModifyPartController {
    @FXML
    private Label partSourceLabel;
    @FXML
    private TextField partIDText, partNameText, partInvText, partPriceOrCostText,
	partInvMaxText, partInvMinText, partSourceText;
    @FXML
    private RadioButton inHousePartButton, outsourcedPartButton;

    private Part updatedPart;
    private int partIndex;


    /**
     * Initializes form fields and sets version using partToModify data.
     * @param partToModify
     * @param partIdx
     * @param <T>
     */
    public <T extends Part> void initForm(T partToModify, int partIdx) {
	partIDText.setText(Integer.toString(partToModify.getId()));
	partNameText.setText(partToModify.getName());
	partInvText.setText(Integer.toString(partToModify.getStock()));
	partPriceOrCostText.setText(Double.toString(partToModify.getPrice()));
	partInvMaxText.setText(Integer.toString(partToModify.getMax()));
	partInvMinText.setText(Integer.toString(partToModify.getMin()));

	if (partToModify instanceof InHouse) {
	    inHousePartButton.setSelected(true);
	    partSourceText.setText(Integer.toString(((InHouse) partToModify).getMachineId()));
	} else if (partToModify instanceof Outsourced) {
	    outsourcedPartButton.setSelected(true);
	    partSourceText.setText(((Outsourced) partToModify).getCompanyName());
	}

	partIndex = partIdx;
    }

    /**
     * Uses form data to set the part copy's fields, validates it,
     * and replaces the original part.
     * @param event
     */
    public void updatePart(ActionEvent event) {
	try {
	    if (inHousePartButton.isSelected()) {
		updatedPart = new InHouse(
		    Integer.parseInt(partIDText.getText()),
		    partNameText.getText(),
		    Double.parseDouble((partPriceOrCostText.getText())),
		    Integer.parseInt(partInvText.getText()),
		    Integer.parseInt(partInvMinText.getText()),
		    Integer.parseInt(partInvMaxText.getText()),
		    Integer.parseInt(partSourceText.getText())
		);
	    } else {
		updatedPart = new Outsourced(
		    Integer.parseInt(partIDText.getText()),
		    partNameText.getText(),
		    Double.parseDouble((partPriceOrCostText.getText())),
		    Integer.parseInt(partInvText.getText()),
		    Integer.parseInt(partInvMinText.getText()),
		    Integer.parseInt(partInvMaxText.getText()),
		    partSourceText.getText()
		);
	    }

	    Utilities.validatePartInventory(updatedPart);
	    Inventory.updatePart(partIndex, updatedPart);
	    returnToMainPage(event);
	}
	catch (NumberFormatException e) {
	    Exceptions.displayNumberFormattingErrorAlert(e);
	}
	catch (Exception e) {
	    Exceptions.displayErrorAlert(e);
	}
    }

    /**
     * Toggle between OutSourced and InHouse forms.
     * @param event
     */
    public void changePartSource(ActionEvent event) {
	RadioButton eventSrc = (RadioButton) event.getSource();
	if (eventSrc == inHousePartButton) {
	    partSourceLabel.setText("Machine ID");
	} else if (event.getSource() == outsourcedPartButton) {
	    partSourceLabel.setText("Company Name");
	}
    }

    /**
     * Goes back to the Main Screen.
     * @param event
     */
    public void returnToMainPage(ActionEvent event) {
	Utilities.navigateToNewPage(event, "/christopherlim/c482/view/MainScreen.fxml");
    }
}
