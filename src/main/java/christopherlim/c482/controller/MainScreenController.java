package christopherlim.c482.controller;

import christopherlim.c482.model.Part;
import christopherlim.c482.model.Product;
import christopherlim.c482.helper.Exceptions;
import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.Inventory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.scene.Node;
import javafx.scene.Scene;


import java.io.IOException;

/**
 * Controller for Main Screen
 * @author Christopher Lim
 */
public class MainScreenController {
    @FXML
    private TextField partsSearchBox, productsSearchBox;
    @FXML
    private TableView<Part> partsInventory;
    @FXML
    private TableView<Product> productsInventory;

    private Stage stage;

    /**
     * Loads the Main Screen method
     */
    @FXML
    public void initialize() {
        partsInventory.setItems(Inventory.getAllParts());
        Utilities.formatTable(partsInventory);

        productsInventory.setItems(Inventory.getAllProducts());
        Utilities.formatTable(productsInventory);
    }

    /**
     * Exits the application.
     * @param event
     */
    public void exit(ActionEvent event) {
        stage = (Stage)((Node)event.getSource()).getScene().getWindow();
        stage.close();
    }

    /**
     * The scene changes when the Add Part Form loads.
     * @param event
     */
    public void loadAddPartForm(ActionEvent event) {
        Utilities.navigateToNewPage(event, "/christopherlim/c482/view/AddPart.fxml");
    }

    /**
     * Loads Add Product Form and changes scene.
     * @param event
     */
    public void loadAddProductForm(ActionEvent event) {
        Utilities.navigateToNewPage(event, "/christopherlim/c482/view/AddProduct.fxml");
    }

    /**
     * <p>
     * RUNTIME ERROR: Pressing "modify" without a row selected caused a NullPointerException.
     * We currently capture that issue and display a row selection alert.
     * Launches the modify part form and loads the selected row.
     * Null partToModify caused the problem in ModifyPartController.initForm, but it started here.
     * </p>
     *
     * Updates the scene by loading the Modify Part Form and passing part data.
     * @param event
     */
    public void loadModifyPartForm(ActionEvent event) {
        Part partToModify = partsInventory.getSelectionModel().getSelectedItem();

        try {
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            if(partToModify == null) {
                Exceptions.displayMissingRowSelectionAlert();
                return;
            }

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/christopherlim/c482/view/ModifyPart.fxml"));
            stage.setScene(new Scene(fxmlLoader.load()));
            int partIdx = partsInventory.getSelectionModel().getSelectedIndex();
            ModifyPartController modifyPart = fxmlLoader.getController();
            modifyPart.initForm(partToModify, partIdx);
            stage.show();
        }
        catch (IOException e) {
            Exceptions.displayIOExceptionAlert(e);
        }
    }

    /**
     * Passes product data to the Modify Product Form and updates the scene.
     * @param event
     */
    public void loadModifyProductForm(ActionEvent event) {
        Product productToModify = productsInventory.getSelectionModel().getSelectedItem();

        try {
            stage = (Stage) ((Node) event.getSource()).getScene().getWindow();

            if(productToModify == null) {
                Exceptions.displayMissingRowSelectionAlert();
                return;
            }

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/christopherlim/c482/view/ModifyProduct.fxml"));
            stage.setScene(new Scene(fxmlLoader.load()));
            int productIndex = productsInventory.getSelectionModel().getFocusedIndex();
            ModifyProductController modifyProduct = fxmlLoader.getController();
            modifyProduct.initForm(productToModify, productIndex);
            stage.show();
        }
        catch (IOException e) {
            Exceptions.displayIOExceptionAlert(e);
        }
    }

    /**
     * Removes the row from the parts table.
     */
    public void deletePart() {
        Part partToDelete = partsInventory.getSelectionModel().getSelectedItem();

        if(partToDelete == null) {
            Exceptions.displayMissingRowSelectionAlert();
            return;
        }

        if (Utilities.confirmUserAction("part deletion")) {
            if (Inventory.deletePart(partToDelete)) {
                Utilities.displayAlert("Part deleted successfully");
            }
        }

    }

    /**
     * Removes the selected product row from the table.
     */
    public void deleteProduct() {
        Product productToDelete = productsInventory.getSelectionModel().getSelectedItem();

        if(productToDelete == null) {
            Exceptions.displayMissingRowSelectionAlert();
            return;
        }

        if (Utilities.confirmUserAction("product deletion")) {
            if (Inventory.deleteProduct(productToDelete)) {
                Utilities.displayAlert("Product deleted successfully.");
            } else {
                Utilities.displayAlert("Product not deleted because at least one part is still associated with it");
            }
        }

    }

    /**
     * Filters by Parts Name and Id
     */
    public void filterParts() {
        String searchText = partsSearchBox.getText();

        try {
            int searchID = Integer.parseInt(searchText);
            filterPartsByID(searchID);
        }
        catch (NumberFormatException e) {
            filterPartsByName(searchText);
        }
    }

    /**
     * Filters by Products Name and Id
     */
    public void filterProducts() {
        String searchText = productsSearchBox.getText();

        try {
            int searchID = Integer.parseInt(searchText);
            filterProductsByID(searchID);
        }
        catch (NumberFormatException e) {
            filterProductsByName(searchText);
        }
    }

    /**
     * Applying a name filter to available parts limits
     * the table to parts with the provided text.
     * @param searchName
     */
    private void filterPartsByName(String searchName) {

        ObservableList<Part> foundParts;
        try {
            foundParts = Inventory.lookupPart(searchName);
            partsInventory.setItems(foundParts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }

    /**
     * Updates the table by filtering products by name
     * to show only those with the provided text.
     * @param searchName
     */
    private void filterProductsByName(String searchName) {

        ObservableList<Product> foundProducts;
        try {
            foundProducts = Inventory.lookupProduct(searchName);
            productsInventory.setItems(foundProducts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }

    /**
     * Filtering parts by ID updates the table to only contain matched components.
     * @param searchID
     */
    private void filterPartsByID(int searchID) {
        Part foundPart;

        ObservableList<Part> foundParts = FXCollections.observableArrayList();
        try {
            foundPart = Inventory.lookupPart(searchID);
            foundParts.add(foundPart);
            partsInventory.setItems(foundParts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }

    /**
     * Filters the table by ID to show only products with matching IDs.
     * @param searchID
     */
    private void filterProductsByID(int searchID) {
        Product foundProduct;

        ObservableList<Product> foundProducts = FXCollections.observableArrayList();
        try {
            foundProduct = Inventory.lookupProduct(searchID);
            foundProducts.add(foundProduct);
            productsInventory.setItems(foundProducts);
        }
        catch (Exception e) {
            Exceptions.displayInformationAlert(e);
        }
    }
}
