package christopherlim.c482.controller;

import christopherlim.c482.helper.Exceptions;
import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.Inventory;
import christopherlim.c482.model.Part;
import christopherlim.c482.model.Product;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;

/**
 * Controller for Add Product
 * @author Christopher Lim
 */
public class AddProductController {
    @FXML
    private TableView<Part> availablePartsTable;
    @FXML
    private TableView<Part> associatedPartsTable;
    @FXML
    private TextField productNameText, productStockText, productPriceOrCostText, productStockMaxText, productStockMinText, partsSearchBox;
    final private ObservableList<Part> associatedParts = FXCollections.observableArrayList();

    /**
     * Loads the Product method
     * */
    public void initialize() {
	availablePartsTable.setItems(Inventory.getAllParts());
	Utilities.formatTable(availablePartsTable);

	associatedPartsTable.setItems(associatedParts);
	Utilities.formatTable(associatedPartsTable);
    }

    /**
     * Uses the search box text to determine whether to search
     * by ID number or part name depending on integer parsing.
     */
    public void filterParts() {
	String searchText = partsSearchBox.getText();

	try {
	    int searchID = Integer.parseInt(searchText);
	    filterPartsByID(searchID);
	}
	catch (NumberFormatException e) {
	    filterPartsByName(searchText);
	}
    }

    /**
     * Filters accessible parts by name, updating the
     * table with just text-containing components.
     * @param searchName
     */
    private void filterPartsByName(String searchName) {

	ObservableList<Part> foundParts;
	try {
	    foundParts = Inventory.lookupPart(searchName);
	    availablePartsTable.setItems(foundParts);
	}
	catch (Exception e) {
	    Exceptions.displayInformationAlert(e);
	}
    }

    /**
     * Filters parts by ID, updating the table with just matching parts.
     * @param searchID
     */
    private void filterPartsByID(int searchID) {
	Part foundPart;

	ObservableList<Part> foundParts = FXCollections.observableArrayList();
	try {
	    foundPart = Inventory.lookupPart(searchID);
	    foundParts.add(foundPart);
	    availablePartsTable.setItems(foundParts);
	}
	catch (Exception e) {
	    Exceptions.displayInformationAlert(e);
	}
    }

    /**
     * The linked parts table receives selected part.
     */
    public void addAssociatedPart() {
	Part associatedPart = availablePartsTable.getSelectionModel().getSelectedItem();
	associatedParts.add(associatedPart);
    }

    /**
     * Removes the selected part.
     */
    public void removeAssociatedPart() {
	Part partToRemove = associatedPartsTable.getSelectionModel().getSelectedItem();

	if (Utilities.confirmUserAction("part disassociation")) {
	    associatedParts.remove(partToRemove);
	}
    }

    /**
     * Calls productFactory and returns to main page if product is valid.
     * @param event
     */
    public void createProduct(ActionEvent event) {
	boolean productCreated = productFactory();

	if (productCreated) {
	    returnToMainPage(event);
	}
    }

    /**
     * Validates the form data and builds a new Product
     * from it and the parts table. Adds new product to inventory.
     * @return A boolean checks if Product is valid
     */
    private boolean productFactory() {
	String newProductName;
	int newProductStock;
	double newProductPriceOrCost;
	int newProductInvMin;
	int newProductInvMax;

	try {
	    newProductName = productNameText.getText();
	    newProductStock = Integer.parseInt(productStockText.getText());
	    newProductPriceOrCost = Double.parseDouble(productPriceOrCostText.getText());
	    newProductInvMax = Integer.parseInt(productStockMaxText.getText());
	    newProductInvMin = Integer.parseInt(productStockMinText.getText());

	    Utilities.validateProductInventory(newProductStock, newProductInvMin, newProductInvMax);

	    Product newProduct = new Product(
		Utilities.getNextAvailableProductID(),
		newProductName,
		newProductPriceOrCost,
		newProductStock,
		newProductInvMin,
		newProductInvMax
	    );

	    for (Part p : associatedParts) {
		newProduct.addAssociatedPart(p);
	    }

	    Inventory.addProduct(newProduct);
	    return true;
	}
	catch (NumberFormatException e) {
	    Exceptions.displayNumberFormattingErrorAlert(e);
	    return false;
	}
	catch (Exception e) {
	    Exceptions.displayErrorAlert(e);
	    return false;
	}
    }

    /**
     * Returns back to the Main Screen.
     * @param event
     */
    public void returnToMainPage(ActionEvent event) {
	Utilities.navigateToNewPage(event, "/christopherlim/c482/view/MainScreen.fxml");
    }
}
