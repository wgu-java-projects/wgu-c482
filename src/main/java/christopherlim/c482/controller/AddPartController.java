package christopherlim.c482.controller;

import christopherlim.c482.helper.Exceptions;
import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.InHouse;
import christopherlim.c482.model.Inventory;
import christopherlim.c482.model.Outsourced;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;

/**
 * Controller for Add Part
 * @author Christopher Lim
 */
public class AddPartController {
    @FXML
    Label partIDLabel, partNameLabel, partInvLabel, partPriceOrCostLabel,
	partInvMinLabel, partInvMaxLabel, partSourceLabel;
    @FXML
    TextField partIDText, partNameText, partInvText, partPriceOrCostText,
	partInvMaxText, partInvMinText, partSourceText;
    @FXML
    private RadioButton inHousePartButton, outsourcedPartButton;


    /**
     * Switch between OutSourced and InHouse forms.
     * @param event
     */
    public void changePartSource(ActionEvent event) {
	RadioButton eventSrc = (RadioButton) event.getSource();
	if (eventSrc == inHousePartButton) {
	    partSourceLabel.setText("Machine ID");
	} else if (event.getSource() == outsourcedPartButton) {
	    partSourceLabel.setText("Company Name");
	}
    }

    /**
     * Gets form data, verifies it, produces a new part, and adds it to inventory.
     * @param event
     */
    public void createPart(ActionEvent event) {
	try {
	    String partName = partNameText.getText();
	    int partStock = Integer.parseInt(partInvText.getText());
	    double partPriceOrCost = Double.parseDouble(partPriceOrCostText.getText());
	    int partInvMax = Integer.parseInt(partInvMaxText.getText());
	    int partInvMin = Integer.parseInt(partInvMinText.getText());

	    Utilities.validatePartInventory(partStock, partInvMin, partInvMax);

	    if (inHousePartButton.isSelected()) {
		int partSource = Integer.parseInt(partSourceText.getText());

		InHouse newPart = new InHouse(Utilities.getNextAvailablePartID(), partName, partPriceOrCost, partStock, partInvMin, partInvMax, partSource);
		Inventory.addPart(newPart);
	    } else if (outsourcedPartButton.isSelected()) {
		String partSource = partSourceText.getText();
		Outsourced newPart = new Outsourced(Utilities.getNextAvailablePartID(), partName, partPriceOrCost, partStock, partInvMin, partInvMax, partSource);
		Inventory.addPart(newPart);
	    }

	    returnToMainPage(event);
	}
	catch (NumberFormatException e) {
	    Exceptions.displayNumberFormattingErrorAlert(e);
	}
	catch (Exception e) {
	    Exceptions.displayErrorAlert(e);
	}
    }

    /**
     * The scene returns to the main page.
     * @param event
     */
    public void returnToMainPage(ActionEvent event) {
	Utilities.navigateToNewPage(event, "/christopherlim/c482/view/MainScreen.fxml");
    }
}
