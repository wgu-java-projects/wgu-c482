package christopherlim.c482.helper;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import christopherlim.c482.model.Part;
import christopherlim.c482.model.Product;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.Optional;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * An abstract class that contains utility methods
 * @author Christopher Lim
 */
public abstract class Utilities {
    private static AtomicInteger nextAvailablePartID = new AtomicInteger(1);
    private static AtomicInteger nextAvailableProductID = new AtomicInteger(1);

    /**
     * Generates new part id
     * @return generateNewPartId
     */
    public static int getNextAvailablePartID() {
	return nextAvailablePartID.getAndIncrement();
    }

    /**
     * Generates new product id
     * @return generateNewProductId
     */
    public static int getNextAvailableProductID() {
	return nextAvailableProductID.getAndIncrement();
    }

    /**
     * Formats any application table since they have similar column data formats.
     * @param table The table to format
     * @param <T> A generic class representing Product or Part based on the table
     */
    public static <T> void formatTable(TableView<T> table) {
	ObservableList<TableColumn<T, ?>> tableColumns = table.getColumns();

	TableColumn<T, Integer> idCol = (TableColumn<T, Integer>) tableColumns.get(0);
	TableColumn<T, String> nameCol = (TableColumn<T, String>) tableColumns.get(1);
	TableColumn<T, Integer> stockCol = (TableColumn<T, Integer>) tableColumns.get(2);
	TableColumn<T, Number> priceCol = (TableColumn<T, Number>) tableColumns.get(3);

	idCol.setCellValueFactory(new PropertyValueFactory<>("id"));
	nameCol.setCellValueFactory(new PropertyValueFactory<>("name"));
	stockCol.setCellValueFactory(new PropertyValueFactory<>("stock"));
	priceCol.setCellValueFactory(new PropertyValueFactory<>("price"));

	priceCol.setCellFactory(cell -> new TableCell<>() {
	    @Override
	    protected void updateItem(Number price, boolean empty) {
		super.updateItem(price, empty);
		NumberFormat formatter = NumberFormat.getCurrencyInstance();
		if (empty) {
		    setText(null);
		} else {
		    setText(formatter.format(price));
		}
	    }
	});

    }

    /**
     * Moves to the FXML file's application page.
     * @param event
     * @param fxmlFile
     */
    public static void navigateToNewPage(ActionEvent event, String fxmlFile) {

	Stage stage;

	try {
	    FXMLLoader fxmlLoader = new FXMLLoader(Utilities.class.getResource(fxmlFile));
	    Scene addPartForm = new Scene(fxmlLoader.load());
	    stage = (Stage) ((Node)event.getSource()).getScene().getWindow();
	    stage.setScene(addPartForm);
	    stage.show();
	}
	catch (IOException e) {
	    Exceptions.displayIOExceptionAlert(e);
	}
    }

    /**
     * Making a shallow replica of a product and its parts.
     * @param productToCopy
     * @return newProduct
     */
    public static Product copyProduct(Product productToCopy) {

	Product newProduct = new Product(
	    productToCopy.getId(),
	    productToCopy.getName(),
	    productToCopy.getPrice(),
	    productToCopy.getStock(),
	    productToCopy.getMin(),
	    productToCopy.getMax()
	);

	for (Part p : productToCopy.getAllAssociatedParts()) {
	    newProduct.addAssociatedPart(p);
	}

	return newProduct;
    }

    /**
     * Shows a generic information alert with a message.
     * @param message
     */
    public static void displayAlert(String message) {
	Alert alert = new Alert(Alert.AlertType.INFORMATION);
	alert.setContentText(message);
	alert.showAndWait();
    }

    /**
     * Makes sure a part has a minimum stock larger than zero and a current
     * stock greater than the minimum but less than the maximum.
     * @param part
     * @throws Exception
     */
    public static void validatePartInventory(Part part) throws Exception {
	if (part.getStock() < part.getMin() || part.getStock() > part.getMax()) {
	    throw new Exception("Part stock must be between the inventory bounds");
	}

	if (part.getMin() < 0) {
	    throw new Exception("Minimum stock must be at least 0");
	}
    }

    /**
     * Checks form entries for part inventory accuracy.
     * @param stock
     * @param min
     * @param max
     * @throws Exception
     */
    public static void validatePartInventory(int stock, int min, int max) throws Exception {
	if (max < min) {
	    throw new Exception("Maximum stock must be greater than minimum stock");
	}

	if (stock < min || stock > max) {
	    throw new Exception("Part stock must be between the inventory bounds");
	}

	if (min < 0) {
	    throw new Exception("Minimum stock must be at least 0");
	}
    }

    /**
     * Ensures a product has a minimum stock larger than zero and
     * a current stock greater than the minimum but less than the maximum.
     * @param product
     * @throws Exception
     */
    public static void validateProductInventory(Product product) throws Exception {
	if (product.getStock() < product.getMin() || product.getStock() > product.getMax()) {
	    throw new Exception("Part stock must be between the inventory bounds");
	}

	if (product.getMin() < 0) {
	    throw new Exception("Minimum stock must be at least 0");
	}

	if (product.getMax() < product.getMin()) {
	    throw new Exception("Maximum stock must be greater than minimum stock");
	}
    }

    /**
     * Verifies that form entries match product inventories.
     * @param stock
     * @param min
     * @param max
     * @throws Exception
     */
    public static void validateProductInventory(int stock, int min, int max) throws Exception {
	if (max < min) {
	    throw new Exception("Maximum stock must be greater than minimum stock");
	}

	if (stock < min || stock > max) {
	    throw new Exception("Product stock must be between the inventory bounds");
	}

	if (min < 0) {
	    throw new Exception("Minimum stock must be at least 0");
	}
    }

    /**
     * Displays a confirmation window for prior actions.
     * @param message
     * @return option.isPresent() and option.get()
     */
    public static boolean confirmUserAction(String message) {
	Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
	alert.setHeaderText("Confirm " + message);
	Optional<ButtonType> option = alert.showAndWait();

	return option.isPresent() && option.get() == ButtonType.OK;
    }

}
