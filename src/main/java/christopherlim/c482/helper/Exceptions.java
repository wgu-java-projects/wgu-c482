package christopherlim.c482.helper;

import javafx.scene.control.Alert;
import java.io.IOException;

/**
 * An abstract class that displays Exceptions alerts
 * @author Christopher Lim
 */
public abstract class Exceptions {
    /**
     * Show alerts for IOException
     * @param e
     */
    public static void displayIOExceptionAlert(IOException e) {
	Alert alert = new Alert(Alert.AlertType.ERROR);
	alert.setContentText(e.getMessage());
	alert.showAndWait();
    }

    /**
     * Show alerts when performing an action without
     * selecting a row in the table
     */
    public static void displayMissingRowSelectionAlert() {
	Alert alert = new Alert(Alert.AlertType.ERROR);
	alert.setContentText("You must select a row");
	alert.showAndWait();
    }

    /**
     * Show alerts for information
     * @param e
     */
    public static void displayInformationAlert(Exception e) {
	Alert alert = new Alert(Alert.AlertType.INFORMATION);
	alert.setContentText(e.getMessage());
	alert.showAndWait();
    }

    /**
     * Show alerts for any errors
     * @param e
     */
    public static void displayErrorAlert(Exception e) {
	Alert alert = new Alert(Alert.AlertType.ERROR);
	alert.setContentText(e.getMessage());
	alert.showAndWait();
    }

    /**
     * Show alerts for number formatting
     * @param e
     */
    public static void displayNumberFormattingErrorAlert(Exception e) {
	Alert alert = new Alert(Alert.AlertType.ERROR);
	alert.setHeaderText("Number formatting error");
	alert.setContentText(e.getMessage());
	alert.showAndWait();
    }
}
