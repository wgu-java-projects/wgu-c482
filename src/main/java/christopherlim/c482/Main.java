package christopherlim.c482;

import christopherlim.c482.helper.Utilities;
import christopherlim.c482.model.InHouse;
import christopherlim.c482.model.Inventory;
import christopherlim.c482.model.Outsourced;
import christopherlim.c482.model.Product;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;


/**
 * WGU C482 - Inventory Management System
 * <p>
 * FUTURE ENHANCEMENT: I would add item and product field history to the next version of the app
 * to improve functionality. Knowing long-term trends in outsourced part costs, inventory levels,
 * and a record of deleted parts and items for future reference can be very helpful.
 * The pricing history could alert management to escalating prices and encourage material
 * or supplier searches. Inventory history can indicate management which months a part or product
 * is needed more and allow them to adjust production or purchasing. Deprecated parts records allow
 * the company to examine historical procurement and production trends to decide whether components
 * or products may be valuable in the future.
 * </p>
 * @author Christopher Lim
 */
public class Main extends Application {
    /**
     * Loads the MainScreen.fxml to start the Application and display the Main Screen
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
	addTestData();


	FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/christopherlim/c482/view/MainScreen.fxml"));
	Scene scene = new Scene(fxmlLoader.load());
	stage.setTitle("C482 - Inventory Management System");
	stage.setScene(scene);
	stage.setResizable(false);
	stage.show();

    }

    private void addTestData() {
	InHouse cases = new InHouse(Utilities.getNextAvailablePartID(), "Cases", 72.00, 152, 0, 500, 1);
	Outsourced cpu = new Outsourced(Utilities.getNextAvailablePartID(), "CPU", 2300.00, 7, 0, 25, "AMD");
	InHouse motherBoard = new InHouse(Utilities.getNextAvailablePartID(), "Mother Board", 540.00, 12, 0, 50, 2);
	Outsourced cpuFan = new Outsourced(Utilities.getNextAvailablePartID(), "Cpu Fan", 30.00, 23, 0, 90, "Asus");
	InHouse lights = new InHouse(Utilities.getNextAvailablePartID(), "Lights (green)", 15.00, 50, 0, 420, 3);
	Inventory.addPart(cases);
	Inventory.addPart(cpu);
	Inventory.addPart(motherBoard);
	Inventory.addPart(cpuFan);
	Inventory.addPart(lights);
	Product desktop = new Product(Utilities.getNextAvailableProductID(), "Desktop", 4000.00, 2, 1, 40);
	Product laptop = new Product(Utilities.getNextAvailableProductID(), "Laptop", 400.00, 13, 0, 35);
	desktop.addAssociatedPart(cases);
	desktop.addAssociatedPart(cpu);
	desktop.addAssociatedPart(lights);
	desktop.addAssociatedPart(motherBoard);
	laptop.addAssociatedPart(cases);
	laptop.addAssociatedPart(cpuFan);
	laptop.addAssociatedPart(lights);
	Inventory.addProduct(desktop);
	Inventory.addProduct(laptop);
    }

    public static void main(String[] args) {
	launch(args);
    }
}
