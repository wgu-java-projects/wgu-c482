package christopherlim.c482.model;

/**
 * Class to extend abstract Part class for In-house Parts
 * @author Christopher Lim
 */
public class InHouse extends Part {
    private int machineId;

    public InHouse(int id, String name, double price, int stock, int min, int max, int machineId) {
	super(id, name, price, stock, min, max);
	this.machineId = machineId;
    }

    /**
     * Gets the machine id
     * @return machineId
     */
    public int getMachineId() {
	return machineId;
    }

    /**
     * Sets the machine id
     * @param machineId
     */
    public void setMachineId(int machineId) {
	this.machineId = machineId;
    }
}
