package christopherlim.c482.model;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 * Class to create Product
 * @author Christopher Lim
 */
public class Product {
    final private ObservableList<Part> associatedParts;
    private int id, stock, min, max;
    private String name;
    private double price;

    public Product(int id, String name, double price, int stock, int min, int max) {
	this.id = id;
	this.name = name;
	this.price = price;
	this.stock = stock;
	this.min = min;
	this.max = max;
	this.associatedParts = FXCollections.observableArrayList();
    }

    /**
     * Gets the associatedParts
     * @return associatedParts
     */
    public ObservableList<Part> getAssociatedParts() {
	return associatedParts;
    }

    /**
     * Gets the id
     * @return id
     */
    public int getId() {
	return id;
    }

    /**
     * Sets the id
     * @param id
     */
    public void setId(int id) {
	this.id = id;
    }

    /**
     * Gets the name
     * @return name
     */
    public String getName() {
	return name;
    }

    /**
     * Sets the name
     * @param name
     */
    public void setName(String name) {
	this.name = name;
    }

    /**
     * Gets the price
     * @return price
     */
    public double getPrice() {
	return price;
    }

    /**
     * Sets the price
     * @param price
     */
    public void setPrice(double price) {
	this.price = price;
    }

    /**
     * Gets the stock
     * @return stock
     */
    public int getStock() {
	return stock;
    }

    /**
     * Sets the stock
     * @param stock
     */
    public void setStock(int stock) {
	this.stock = stock;
    }

    /**
     * Gets the min
     * @return min
     */
    public int getMin() {
	return min;
    }

    /**
     * Sets the min
     * @param min
     */
    public void setMin(int min) {
	this.min = min;
    }

    /**
     * Gets the max
     * @return max
     */
    public int getMax() {
	return max;
    }

    /**
     * Sets the max
     * @param max
     */
    public void setMax(int max) {
	this.max = max;
    }

    /**
     * Adds a part to the list of parts associated with the product
     * @param part
     */
    public void addAssociatedPart(Part part) {
	associatedParts.add(part);
    }

    /**
     * Deletes the given part from the list of parts associated with the product
     * @param selectedAssociatedPart
     */
    public void deleteAssociatedPart(Part selectedAssociatedPart) {
	associatedParts.remove(selectedAssociatedPart);
    }

    /**
     * Returns a list of all parts associated with the product
     * @return associatedParts
     */
    public ObservableList<Part> getAllAssociatedParts() {
	return associatedParts;
    }
}
