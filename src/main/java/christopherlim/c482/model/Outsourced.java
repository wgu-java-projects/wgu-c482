package christopherlim.c482.model;

/**
 * Class to extend abstract Part class for Outsourced Parts
 * @author Christopher Lim
 */
public class Outsourced extends Part{
    private String companyName;

    public Outsourced(int id, String name, double price, int stock, int min, int max, String companyName) {
	super(id, name, price, stock, min, max);
	this.companyName = companyName;
    }

    /**
     * Gets the company name
     * @return companyName
     */
    public String getCompanyName() {
	return companyName;
    }

    /**
     * Sets the company name
     * @param companyName
     */
    public void setCompanyName(String companyName) {
	this.companyName = companyName;
    }
}
