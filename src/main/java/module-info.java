module christopherlim.c482 {
    requires javafx.controls;
    requires javafx.fxml;

    opens christopherlim.c482.controller to javafx.fxml;
    exports christopherlim.c482;
    exports christopherlim.c482.model;
    exports christopherlim.c482.controller;
    exports christopherlim.c482.helper;
}